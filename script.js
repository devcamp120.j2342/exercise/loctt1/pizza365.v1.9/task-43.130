"use strict"
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gCOL_NAME = ["orderCode", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
const gCOL_ORDER_CODE = 0;
const gCOL_KICH_CO = 1;
const gCOL_LOAI_PIZZA = 2;
const gCOL_NUOC_UONG = 3;
const gCOL_THANH_TIEN = 4;
const gCOL_HO_TEN = 5;
const gCOL_SO_DIEN_THOAI = 6;
const gCOL_TRANG_THAI = 7;
const gCOL_ACTION = 8;
const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders"
var gDataOrder = "";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    onLoadDataTable();
    $("#table-user").on("click", ".btn-detail", function () {
        gDataOrder = getDataRowTable(this);
        console.log(gDataOrder);
        $("#modal-detail").modal().show;
        onBtnDetail();
    })
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $("#table-user").DataTable({
        columns: [
            { "data": gCOL_NAME[gCOL_ORDER_CODE] },
            { "data": gCOL_NAME[gCOL_KICH_CO] },
            { "data": gCOL_NAME[gCOL_LOAI_PIZZA] },
            { "data": gCOL_NAME[gCOL_NUOC_UONG] },
            { "data": gCOL_NAME[gCOL_THANH_TIEN] },
            { "data": gCOL_NAME[gCOL_HO_TEN] },
            { "data": gCOL_NAME[gCOL_SO_DIEN_THOAI] },
            { "data": gCOL_NAME[gCOL_TRANG_THAI] },
            { "data": gCOL_NAME[gCOL_ACTION] },

        ],
        columnDefs: [
            {
                targets: gCOL_ACTION,
                defaultContent: '<button class="btn btn-primary btn-detail">Chi tiết</button>'
            }
        ]
    })
}
function onLoadDataTable() {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        success: function (paramRes) {
            loadDataTable(paramRes);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    })
}
function onBtnDetail() {
    var vObjOrder = {
        orderCode: "",
        kichCo: "",
        loaiPizza: "",
        duongKinh: "",
        suon: "",
        salad: "",
        idVourcher: "",
        thanhTien: "",
        giamGia: "",
        idLoaiNuocUong: "",
        soLuongNuocUong: "",
        hoTen: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: "",
        ngayTao: "",
        ngayCapNhat: "",

    }
    getDataModal();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data DataTable
function loadDataTable(paramData) {
    var vTable = $("#table-user").DataTable();
    vTable.clear();
    vTable.rows.add(paramData);
    vTable.draw();
}
function getDataRowTable(paramElement) {

    var vTable = $("#table-user").DataTable();
    var vRow = $(paramElement).closest("tr");
    var vData = vTable.row(vRow).data();
    return vData;
}
// load data modal
function getDataModal() {
    $("#inp-order-code").val(gDataOrder.orderCode);
    $("#inp-kich-co").val(gDataOrder.kichCo);
    $("#inp-duong-kinh").val(gDataOrder.duongKinh);
    $("#inp-suon").val(gDataOrder.suon);
    $("#inp-salad").val(gDataOrder.salad);
    $("#inp-loai-pizza").val(gDataOrder.loaiPizza);
    $("#inp-id-vourcher").val(gDataOrder.idVourcher);
    $("#inp-thanh-tien").val(gDataOrder.thanhTien);
    $("#inp-giam-gia").val(gDataOrder.giamGia);
    $("#inp-loai-do-uong").val(gDataOrder.idLoaiNuocUong);
    $("#inp-so-luong-nuoc").val(gDataOrder.soLuongNuoc);
    $("#inp-ho-ten").val(gDataOrder.hoTen);
    $("#inp-email").val(gDataOrder.email);
    $("#inp-so-dien-thoai").val(gDataOrder.soDienThoai);
    $("#inp-dia-chi").val(gDataOrder.diaChi);
    $("#inp-loi-nhan").val(gDataOrder.loiNhan);
    $("#inp-ngay-tao").val(gDataOrder.ngayTao);
    $("#inp-ngay-sua").val(gDataOrder.ngayCapNhat);

}